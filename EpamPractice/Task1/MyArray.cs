﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class MyArray: IEnumerable
    {
        private int[] array;
        private int _length;
        public int Lenght
        {
            get
            {
                return _length;
            }
            set
            {
                if (value < 1)
                {
                    throw new InvalidOperationException();
                }
                else
                {
                    _length = value;
                    array = new int[Lenght];
                    LastIndex = Lenght + FirstIndex;
                }

            }
        }
        private int _firstIndex;
        public int FirstIndex
        {
            get
            {
                return _firstIndex;
            }

            set
            {
                _firstIndex = value;
                LastIndex = Lenght + FirstIndex;
            }
        }
        public int LastIndex { get; private set; }

        public MyArray()
        {
           
        }
        
        public MyArray(int firstIndex, int lenght)
        {
            Lenght = lenght;
            array = new int[Lenght];
            FirstIndex = firstIndex;
            LastIndex = Lenght + FirstIndex;
        } 
        //overloaded indexer
        public int this[int index]
        {
            get
            {
                if (index < FirstIndex || index > (Lenght + FirstIndex))
                    throw new IndexOutOfRangeException();
                else
                return array[index - FirstIndex];
            }
            set
            {
                array[index - FirstIndex] = value;
            }
        }

        public void AddArrays(MyArray outcome)
        {
            if(!CheckInterval(this,outcome))
            {
                throw new InvalidOperationException();
            }
            else
            {
                for(int i = this.FirstIndex; i < this.LastIndex; i++)
                {
                    this[i] += outcome[i];
                }
            }
        }

        public static MyArray AddArrays(MyArray first,MyArray second)
        {
            if (!(first.Lenght == second.Lenght && first.FirstIndex == second.FirstIndex))
            {
                throw new InvalidOperationException();
            }
            MyArray result = new MyArray(first.FirstIndex,first.Lenght);
            for(int i = result.FirstIndex; i < result.LastIndex; i++)
            {
                result[i] = first[i] + second[i];
            }
            return result;
        }

        public void Substract(MyArray outcome)
        {
            if (!CheckInterval(this, outcome))
            {
                throw new InvalidOperationException();
            }
            else
            {
                for (int i = this.FirstIndex; i < this.LastIndex; i++)
                {
                    this[i] -= outcome[i];
                }
            }
        }

        public void MultiplyOnScalar(int scalar)
        {
            for (int i = this.FirstIndex; i < this.LastIndex; i++)
            {
                this[i] *= scalar;
            }
        }



        public bool SequenceEquals(MyArray toCheck)
        {
            return array.SequenceEqual(toCheck.array);
        }

        

        private bool CheckInterval(MyArray first, MyArray second)
        {
            return (first.Lenght == second.Lenght && first.FirstIndex == second.FirstIndex);
        }

        public IEnumerator GetEnumerator()
        {
            return array.GetEnumerator();
        }

        public override string ToString()
        {
            string result = "";
            foreach (var element in this.array)
                result += element.ToString() + " ";
            return result;
        }
    }
}
